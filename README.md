# Documentation

Documentation for the microservices project of Cloud Computing 2018.

## IP Adress of my microservices
http://35.244.166.15

## Overview

The following diagram depicts from a high level perspectiv overall my CI/CD flow.
This flow is applied to the three microservices:
 - watches-v1
 - watches-v2
 - images-v1

![Alt text](./CI-CD-Flow.PNG)

 ##### Note: The step to re-initialize the database is not applied for the microservice images-v1.
 The link to jenkins: http://35.228.46.76:8080/blue/pipelines/ 
 
 Login credentials: user: cloud ; password: computing


 My docker hub account: https://hub.docker.com/u/harperkej/ where the following docker images are published:

 - watches-v1
 - watches-v2
 - images-v1

## Deployment

The following conditions should be fulilled in order to deploy the system:
 - Java 8 is installed properly (JAVA_HOME path is set).
 - A static IP adress with the name "static-ip" should be available in the Google Cloud Platform project (GCP abbreviation from now and on) - or in case there's no such static IP just update properly the file ingress.yaml to point to an existing IP address at your project or optionally delete the line from the file completely.

 ### First step: Build the project

   In every project there exists a shell script called **build-project.sh** which is used to build the project. This script fails if the first condition is not properly fulfilled(java properly installed). So, the first step is to build the project using this script. The script should be executed in every project.

### Second step: Publish the docker images to docker hub

   After that, the script **publish-docker-image.sh** is used to publish the image to my docker hub repository. This script will publish the docker image with tag based on what is the content of the file **version.txt** - **the first time the deployment is done, the version should be set to 1**. The script should be executed in every project.

### Third step: Deployment

  After this docker image is published, the script **init-deployment.sh** should be executed to create the deployment object in Google Kubernetes Engine cluster (GKE abbreviation from now and on). This script will create a deployment object (based on _microservice_name_-deployment.yaml file) with 3 replicas (2 replicas for the microservice images-v1). The script should be executed in every project. At this point the microservices are deployed but not exposed (deployment objects are created and services of type **NodePort** are created) and can't be access from outside of the cluster. The following paragraph explains how make the microservice accessible from the outside world.


### Fourth step: Publishing microservices  

  After the deployment and services objects are created, it's time to expose the microservices. This can be done using the script **configure-ingress.sh**. This script will create an Ingress controller (based on ingress.yaml file) in GCP project and will configure it to route the request to appropriate microservice:  
  /watches/v1/* --> watches-v1  
  /watches/v2/* --> watches-v2  
  /images/v1/*  --> images-v1  

This script can be execute from the script in all projects - it is the same in all projects. But is should be execute only after all microservices are deployed (the third step). 

### Re-deployment
 
 In case you need to redeploy any of the microservices undertake the following actions:  
  - Update the version use the script **update_version.sh**. I'll explain why updating the version is necessary*.  
  - Do the changes you want to do and **build-project.sh**
  - **publish-docker-image.sh**
  - **re-deploy.sh**
* Because the command **kubectl set image .... ** will not update the image of the containers -- unless the tag of the image has changed compared to the deployed one.  
This is convenient also in the process of automating the deployment - in case the version is changed the update will take place - otherwise not (we wouldn't want excatly in every commit to deploy new version...).

 ### Database
 
   The database is deployed in GCP and it accepts requests from every host. There's a script in projects watches-v1 and watches-v2 called **setup-database.sh** that executes the SQL script **database.sql** against my database deployment. However, for this script to work **mysql-client** should be installed. Also, the microservices are tighly coupled to the database - so changing it (the schemas - not the data) might impact the microservices. 


 ### General notes
 I didn't cover setting up the GKE cluster and setting up kubectl to work with appropriate cluster.   
 The above scripts should be execute with super user privileges - I had a tough time because of this - especially when executing scripts from Jenkins.
 